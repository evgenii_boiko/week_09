package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    @Autowired
    private ComponentA componentA;

    @Autowired
    private ComponentB componentB;

    public boolean isValid() {
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
