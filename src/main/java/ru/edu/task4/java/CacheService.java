package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component("ru.edu.task4.java.CacheService")
public class CacheService implements SomeInterface {

    private SomeInterface delegate;

    @Autowired
    public CacheService(@Qualifier("ru.edu.task4.java.RealService") SomeInterface delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getName() {
        return "cacheService of " + delegate.getName();
    }
}
