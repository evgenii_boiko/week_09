package ru.edu.task2.java;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Objects;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
@Scope("prototype")
public class TimeKeeper {

    private Instant ts = Instant.now();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeKeeper that = (TimeKeeper) o;
        return Objects.equals(ts, that.ts);
    }

}
