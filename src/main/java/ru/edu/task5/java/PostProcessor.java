package ru.edu.task5.java;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

@Component
public class PostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        if (bean instanceof InterfaceToRemove) {
            return "deletedBean";
        }
        return BeanPostProcessor.super
                .postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        if (bean instanceof InterfaceToWrap) {
            return (InterfaceToWrap) () -> "wrapped "
                    + ((InterfaceToWrap) bean).getValue();
        }
        return bean;
    }
}
