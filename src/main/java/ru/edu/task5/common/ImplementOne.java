package ru.edu.task5.common;

/**
 * ReadOnly
 */
public class ImplementOne implements InterfaceToWrap {
    @Override
    public String getValue() {
        return "one";
    }
}
