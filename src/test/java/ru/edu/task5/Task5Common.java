package ru.edu.task5;

import org.springframework.context.ApplicationContext;
import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * ReadOnly
 */
public class Task5Common {

    public void run(ApplicationContext ctx) {
        Map<String, InterfaceToWrap> wrappedBeans = ctx.getBeansOfType(InterfaceToWrap.class);
        assertEquals(2, wrappedBeans.size());
        for (Map.Entry<String, InterfaceToWrap> entry : wrappedBeans.entrySet()) {
            assertTrue(entry.getKey() + " не вернул корректное значение", entry.getValue().getValue().startsWith("wrapped "));
        }

        Map<String, InterfaceToRemove> beansToRemove = ctx.getBeansOfType(InterfaceToRemove.class);
        assertEquals(0, beansToRemove.size());
    }
}
