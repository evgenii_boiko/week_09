package ru.edu.task5.xml;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import ru.edu.task5.Task5Common;
import ru.edu.task5.common.InterfaceToRemove;
import ru.edu.task5.common.InterfaceToWrap;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppXMLTest extends Task5Common {

    @Test
    public void run() {
        ApplicationContext ctx = AppXML.run();
        run(ctx);
    }
}
